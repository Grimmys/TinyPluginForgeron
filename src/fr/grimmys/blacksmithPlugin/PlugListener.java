package fr.grimmys.blacksmithPlugin;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

public class PlugListener implements Listener {
	ArrayList<BlacksmithItem> stockedItems = new ArrayList<BlacksmithItem>();
	
	@EventHandler
	public void onHurt(EntityDamageEvent event) {
		Entity ent = event.getEntity();
		if (ent.getCustomName() == "Willem") {
			event.setDamage(0);
		}
	}
	
	@EventHandler
	public void onEntityInteraction(PlayerInteractEntityEvent event) {
		Player p = event.getPlayer();
		Entity ent = event.getRightClicked();
		if (ent.getCustomName() == "Willem") {
			event.setCancelled(true);
			PlayerInventory pInv = p.getInventory();
			BlacksmithItem itOnHold = playerObjectOnHold(p.getName());
			if (itOnHold == null) {
				ItemStack itAtHand = pInv.getItemInMainHand();
				if (itAtHand != null) {
					if (itAtHand.getDurability() > 0) {
						Inventory inv = Bukkit.createInventory(null, 27, "�8R�paration de mat�riel");
						ItemStack confirm = new ItemStack(Material.ANVIL, 1);
						ItemMeta confirmM = confirm.getItemMeta();
						confirmM.setDisplayName("Donner l'objet � Willem");
						confirm.setItemMeta(confirmM);
						inv.setItem(10, confirm);
						ItemStack cancel = new ItemStack(itAtHand.getType(), 1);
						ItemMeta cancelM = cancel.getItemMeta();
						cancelM.setDisplayName("Conserver l'objet");
						cancel.setItemMeta(cancelM);
						inv.setItem(16, cancel);
						p.openInventory(inv);
					} else {
						p.sendMessage("�4[Willem] �eSalutations ! Je peux gratuitement r�parer vos biens, mais il me faudra du temps. Si vous vous pointez avec l�objet ad�quat (arme, armure, outil) je m�en occupe !");
					}
				} else {
					p.sendMessage("�4[Willem] �eSalutations ! Je peux gratuitement r�parer vos biens, mais il me faudra du temps. Donnez le moi et je verrai ce que je peux faire.");
				}
			} else {
				int timeLeft = itOnHold.getTimeLeft();
				if (timeLeft > 0) {
					p.sendMessage("�4[Willem] �eSalutations ! Il me reste encore " + timeLeft + " heures environ avant que je je termine ! Vous serez pr�venu quand �a sera fini.");
				} else if (pInv.firstEmpty() != -1) {
					Inventory inv = Bukkit.createInventory(null, 27, "�8R�paration de mat�riel");
					ItemStack confirm = new ItemStack(Material.WOOL, 1, (byte)5);
					ItemMeta confirmM = confirm.getItemMeta();
					confirmM.setDisplayName("R�cup�rer votre objet r�par�");
					confirm.setItemMeta(confirmM);
					inv.setItem(10, confirm);
					ItemStack cancel = new ItemStack(Material.WOOL, 1, (byte)14);
					ItemMeta cancelM = cancel.getItemMeta();
					cancelM.setDisplayName("Le laisser en attente");
					cancel.setItemMeta(cancelM);
					inv.setItem(16, cancel);
					p.openInventory(inv);
				} else {
					p.sendMessage("�4[Willem] �eAh, il me semblerai que vous �tes trop charg�s. Revenez r�cup�rer votre objet quand vous aurez de la place.");
				}
			}
		}
		
	}
	
	@EventHandler
	public void onInventoryInteraction(InventoryClickEvent event) {
		Player p = (Player) event.getWhoClicked();
		Inventory inv = event.getInventory();
		ItemStack it = event.getCurrentItem();
		
		if (it.getItemMeta() == null) {
			return;
		}
		
		if (inv.getName().equalsIgnoreCase("�8R�paration de mat�riel")) {
			event.setCancelled(true);
			String itName = it.getItemMeta().getDisplayName();
			PlayerInventory pInv = p.getInventory();
			
			if (itName.equalsIgnoreCase("Donner l'objet � Willem")) {
				p.sendMessage("�4[Willem] �eD'accord, je vous le prends !");
				ItemStack pItInHand = pInv.getItemInMainHand();
				pInv.setItemInMainHand(null);
				BlacksmithItem newStockedItem = new BlacksmithItem(p.getName(), pItInHand, new Timestamp(System.currentTimeMillis()));
				stockedItems.add(newStockedItem);
				Timer timer = new Timer();
				timer.schedule(new TimerTask() {
					@Override
					public void run() {
							p.sendMessage("�aVotre objet est fini d��tre r�par� !");
					}
				}, newStockedItem.getTimeLeft() * 1000);
			} else if (itName.equalsIgnoreCase("Conserver l'objet")) {
				p.sendMessage("�4[Willem] �ePas de soucis !");
			} else if (itName.equalsIgnoreCase("R�cup�rer votre objet r�par�")) {
				p.sendMessage("�4[Willem] �eVoil� votre objet r�par� !");
				BlacksmithItem itOnHold = playerObjectOnHold(p.getName());
				ItemStack itRecup = itOnHold.getItemStack();
				stockedItems.remove(itOnHold);
				itRecup.setDurability((short)0);
				pInv.addItem(itRecup);
			} else if (itName.equalsIgnoreCase("Le laisser en attente")) {
				p.sendMessage("�4[Willem] �eD�accord, je le garde en attendant, il est toujours ici pr�t � �tre r�cup�r� !");
			}
			
			p.closeInventory();
		}
	}
	
	private BlacksmithItem playerObjectOnHold(String playerName) {
		for (BlacksmithItem it : this.stockedItems) {
			if (it.getPlayerName() == playerName) {
				return it;
			}
		}
		return null;
	}
}
