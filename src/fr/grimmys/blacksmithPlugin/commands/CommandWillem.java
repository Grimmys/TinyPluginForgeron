package fr.grimmys.blacksmithPlugin.commands;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Villager.Profession;

public class CommandWillem implements CommandExecutor {
	
	private Villager willem = null;
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			if (p.hasPermission("willem.use")) {
				if (cmd.getName().equalsIgnoreCase("spawnWillem")) {
					if (this.willem == null) {
						createWillem(p);
					} else {
						moveWillem(p.getLocation());
					}
				} else if (cmd.getName().equalsIgnoreCase("moveWillem")) {
					if (this.willem == null) {
						createWillem(p);
					}
					if (args.length == 0) {
						moveWillem(p.getLocation());
					} else if (args.length == 3){
						moveWillem(new Location(p.getWorld(), Double.parseDouble(args[0]), Double.parseDouble(args[1]), Double.parseDouble(args[2])));
					} else {
						p.sendMessage("�4/moveWillem ou /moveWillem x y z");
					}
				}
			} else {
				p.sendMessage("�4Vous n'avez pas la permission requise");
			}
		}
		return false;
	}
	
	private void createWillem(Player p) {
		p.sendMessage("done");
		this.willem = (Villager) p.getWorld().spawnEntity(p.getLocation(), EntityType.VILLAGER);
		this.willem.setProfession(Profession.BLACKSMITH);
		this.willem.setCustomName("Willem");
	}
	
	private void moveWillem(Location pos) {
		this.willem.teleport(pos);
	}

}
