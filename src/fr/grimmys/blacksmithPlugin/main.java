package fr.grimmys.blacksmithPlugin;

import org.bukkit.plugin.java.JavaPlugin;

import fr.grimmys.blacksmithPlugin.commands.CommandWillem;

public class main extends JavaPlugin {
	
	@Override
	public void onEnable() {
		CommandWillem coWi = new CommandWillem();
		getCommand("spawnWillem").setExecutor(coWi);
		getCommand("moveWillem").setExecutor(coWi);
		getServer().getPluginManager().registerEvents(new PlugListener(), this);
	}

}
