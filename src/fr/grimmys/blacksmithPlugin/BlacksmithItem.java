package fr.grimmys.blacksmithPlugin;

import java.sql.Timestamp;

import org.bukkit.inventory.ItemStack;

public class BlacksmithItem {
	private static int SECONDNEEDED = 50;
	private String playerName;
	private ItemStack it;
	private Timestamp date;
	
	
	public BlacksmithItem(String playerName, ItemStack it, Timestamp date) {
		this.playerName = playerName;
		this.it = it;
		this.date = date;
	}
	
	public String getPlayerName() {
		return this.playerName;
	}
	
	public ItemStack getItemStack() {
		return this.it;
	}
	
	public int getTimeLeft() {
		return SECONDNEEDED - (int)((System.currentTimeMillis() - this.date.getTime()) / 1000);
	}
}
